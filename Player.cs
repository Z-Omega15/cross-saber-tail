﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody2D myRigidbody;

    private Animator myAnimator;

    [SerializeField]
    private float movementSpeed;

    private bool swordattack;

    private bool fireball;

    private bool charge;

    private bool facingRight;

    [SerializeField]
    private Transform[] groundcheck;

    [SerializeField]
    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;

    [SerializeField]
    private bool isGrounded;

    private bool jump;

    [SerializeField]
    private float jumpforce;

    [SerializeField]
    private GameObject fireballPrefab;

    [SerializeField]
    private GameObject chargedfireballPrefab;

	void Start ()
    {
        facingRight = true;
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
	}

    void Update()
    {
        HandleInput();
    }

    void FixedUpdate ()
    {
        float horizontal = Input.GetAxis("Horizontal");

        isGrounded = IsGrounded();

        HandleMovement(horizontal);

        Flip(horizontal);

        HandleAttacks();

        HandleLayers();

        ResetValues();
	}
    private void HandleMovement(float horizontal)
    {
        if(myRigidbody.velocity.y < 0)
        {
            myAnimator.SetBool("land", true);
        }

        if(isGrounded && jump )
        {
            isGrounded = false;
            myRigidbody.AddForce(new Vector2(0,jumpforce));
            myAnimator.SetTrigger("jump");
        }

        if(!this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            myRigidbody.velocity = new Vector2(horizontal * movementSpeed, myRigidbody.velocity.y);
        }

        myAnimator.SetFloat("speed", Mathf.Abs(horizontal));
    }

    private void HandleAttacks()
    {
        if(swordattack && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            myAnimator.SetTrigger("swordattack");
            myRigidbody.velocity = Vector2.zero;
        }
        
        if(fireball)
        {
            myAnimator.SetTrigger("fireball");
            myRigidbody.velocity = Vector2.zero;
            Shootfire(0);
        }

        if(charge)
        {
            myAnimator.SetTrigger("charge");
            myRigidbody.velocity = Vector2.zero;
            Shootcharged(0);
        }
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            swordattack = true;
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            fireball = true;
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            charge = true;
        }
        
    }

    private void Flip(float horiziontal)
    {
        if(horiziontal > 0 && !facingRight || horiziontal < 0 && facingRight)
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;
            transform.localScale = theScale;

        }
    }
    private void ResetValues()
    {
        swordattack = false;

        fireball = false;

        charge = false;

        jump = false;
    }

    private bool IsGrounded()
    {
        if(myRigidbody.velocity.y <= 0)
        {
            foreach (Transform point in groundcheck)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position,groundRadius, whatIsGround);

                for(int i = 0; 1< colliders.Length; i++ )
                {
                    if(colliders[i].gameObject != gameObject)
                    {
                        myAnimator.ResetTrigger("jump");
                        myAnimator.SetBool("land",false);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void HandleLayers()
    {
        if(!isGrounded)
        {
            myAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            myAnimator.SetLayerWeight(1, 0);
        }
    }

    public void Shootfire(int value)
    {
        if(facingRight)
        {
            GameObject tmp = (GameObject)Instantiate(fireballPrefab, transform.position, Quaternion.Euler(new Vector3(0,0,-360)));
            tmp.GetComponent<Fireball>().Initialize(Vector2.right);
        }
        else
        {
            GameObject tmp = (GameObject)Instantiate(fireballPrefab, transform.position, Quaternion.Euler(new Vector3(0, 0, 180)));
            tmp.GetComponent<Fireball>().Initialize(Vector2.left);
        }
    }

    public void Shootcharged(int value)
    {
        if (facingRight)
        {
            GameObject tmp = (GameObject)Instantiate(chargedfireballPrefab, transform.position, Quaternion.Euler(new Vector3(0, 0, -360)));
            tmp.GetComponent<chargedfireball>().Initialize(Vector2.right);
        }
        else
        {
            GameObject tmp = (GameObject)Instantiate(chargedfireballPrefab, transform.position, Quaternion.Euler(new Vector3(0, 0, 180)));
            tmp.GetComponent<chargedfireball>().Initialize(Vector2.left);
        }
    }
}
