﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public Transform target;
    public float chaseRange;

    public float attackRange;
    public int damage;
    private float lastAttackTime;
    public float attackDelay;

    void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, target.position);
        if(distanceToPlayer < attackDelay)
        {
            if(Time.time > lastAttackTime + attackDelay)
            {
                target.SendMessage("TakeDamage", damage);
                lastAttackTime = Time.time;
            }
            
        }
    }
}
