﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalking : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    private float moveInput;

    public float dashspeed;
    private float dashTime;
    public float startdashTime;
    private int direction;

    private Rigidbody2D rb;

    private bool facingRight = true;

    private bool isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;

    private int extraJumps;
    public int extraJumpsValue;

    public LayerMask whatIsWall;
    public Transform Wallcheck;
    private bool wall;

    public GameObject spawnPoint;
    public GameObject player;

    void Start()
    {
        extraJumps = extraJumpsValue;
        rb = GetComponent<Rigidbody2D>();
        WarpToSpawn();
        player = GameObject.FindGameObjectWithTag("Player");
        dashTime = startdashTime;
    }

    public void WarpToSpawn()
    {
        player.transform.position = spawnPoint.transform.position;
    }

    void Update()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        wall = Physics2D.OverlapCircle(Wallcheck.position, checkRadius ,whatIsWall);
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }

        else if (facingRight == true && moveInput<0)
        {
            Flip();
        }

        if(isGrounded == true)
        {
            extraJumps = 2;
        }

        if(wall == false)
        {
            Debug.Log(wall);
        }

        if(wall == true)
        {
            extraJumps = 2;
        }

        if(Input.GetKeyDown(KeyCode.UpArrow) && extraJumps != 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
        }
       
        if(direction == 0)
        {
            if(Input.GetKeyDown(KeyCode.LeftArrow))
            {
                direction = 1;
            }
            else if(Input.GetKeyDown(KeyCode.RightArrow))
            {
                direction = 2;
            }
        }
        else
        {
            if(dashTime <=0)
            {
                direction = 0;
                dashTime = startdashTime;
            }
            else
            {
                dashTime -= Time.deltaTime;
                if(direction == 1)
                {
                    rb.velocity = Vector2.left * dashspeed;
                }
                else if(direction == 2)
                {
                    rb.velocity = Vector2.left * dashspeed;
                }
            }
        }
      
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }
}
