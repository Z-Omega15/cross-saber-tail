﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField]
    Slider healthBar;
    [SerializeField]
    Text healthText;

    public float maxhealth;
    float curHealth;
    public GameObject player;
    private PlayerWalking playerWalkingScript;

    void Start()
    {
        healthBar.value = maxhealth;
        curHealth = healthBar.value;
        player = GameObject.FindGameObjectWithTag("Player");
        playerWalkingScript = GetComponent<PlayerWalking>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Spikes")
        {
            healthBar.value -= 100f;
            curHealth = healthBar.value;
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if(col.gameObject.tag == "Boss" || col.gameObject.tag == "Enemy")
        {
            healthBar.value -= 1.5f;
            curHealth = healthBar.value;
        }

        if(col.gameObject.tag == "Spikes")
        {
            healthBar.value -= 100f;
            curHealth = healthBar.value;
        }
    }

    void FixedUpdate()
    {
        if(curHealth == 0)
        {
            curHealth = healthBar.value;
        }
    }
     void Update()
    {
        if(curHealth == 0)
        {
            if(player)
            {
                playerWalkingScript.WarpToSpawn();
                //transform.position = Vector3.zero;
            }
        }
    }
}
    
